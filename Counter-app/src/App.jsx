import { useState } from "react";
import "./App.css";
import CartItem from "./components/cart-item";

function App({ data }) {
  const [counterData, setCounterData] = useState([...data]);

  function increment(itemId) {
    setCounterData((oldData) => {
      const newData = oldData.map((ele) => {
        if (ele.id == itemId) {
          return { ...ele, quantity: ele.quantity + 1 };
        }
        return ele;
      });
      return newData;
    });
  }
  function decrement(itemId) {
    setCounterData((oldData) => {
      const newData = oldData.map((ele) => {
        if (ele.id == itemId && ele.quantity > 0) {
          return { ...ele, quantity: ele.quantity - 1 };
        }
        return ele;
      });
      return newData;
    });
  }
  function deleteItem(itemId) {
    setCounterData((oldData) => {
      const newData = oldData.filter((ele) => ele.id != itemId);
      return newData;
    });
  }

  function reset() {
    setCounterData((oldData) => {
      return oldData.map((ele) => {
        return { ...ele, quantity: 0 };
      });
    });
  }
  function refresh() {
    if (counterData.length == 0) {
      setCounterData([...data]);
    }
  }

  const noOfItems = counterData.reduce((noOfItems, ele) => {
    if (ele.quantity > 0) {
      ++noOfItems;
    }
    return noOfItems;
  }, 0);
  return (
    <>
      <div id="header">
        <span>
          <i className="fa-solid fa-cart-shopping"></i>
        </span>
        <span id="no-of-items">{noOfItems}</span>
        <span>Items</span>
      </div>

      <div id="header-buttons">
        <button id="reset" onClick={() => reset()}>
          <i className="fa-solid fa-arrows-rotate" style={{color: "#FFFFFF"}}></i>
        </button>
        <button id="refresh" onClick={() => refresh()}>
          <i className="fa-solid fa-recycle" style={{color: "#FFFFFF"}}></i>
        </button>
      </div>
      {counterData.map((element) => {
        return (
          <div id="cart-items">
            <CartItem
              item={element}
              increment={increment}
              decrement={decrement}
              deleteItem={deleteItem}
            />
          </div>
        );
      })}
    </>
  );
}

export default App;
