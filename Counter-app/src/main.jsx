import React from "react";
import ReactDOM from "react-dom/client";
import App from "./App.jsx";

const data = [
  { name: "item-1", id: 0, quantity: 0 },
  { name: "item-2", id: 1, quantity: 0 },
  { name: "item-3", id: 2, quantity: 0 },
  { name: "item-4", id: 3, quantity: 0 },
];

ReactDOM.createRoot(document.getElementById("root")).render(
  <React.StrictMode>
    <App data={data}/>
  </React.StrictMode>
);

