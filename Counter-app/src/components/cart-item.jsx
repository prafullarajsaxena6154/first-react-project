import React from "react";

const CartItem = ({ item, increment, decrement, deleteItem }) => {
  return (
    <>
      <div id='items'>
        <span id="item-quantity" className={item.quantity>0?'blue':'yellow'}>
          {item.quantity <= 0 ? "Zero" : item.quantity}
        </span>
        <button id="add" onClick={() => increment(item.id)}>
          <i
            className="fa-solid fa-circle-plus"
            style={{ color: "#ffffff" }}
          ></i>
        </button>
        <button id="subtract" onClick={() => decrement(item.id)}>
          <i
            className="fa-solid fa-circle-minus"
            style={{ color: "#ffffff" }}
          ></i>
        </button>
        <button id="delete" onClick={() => deleteItem(item.id)}>
          <i className="fa-solid fa-trash-can" style={{ color: "#ffffff" }}></i>
        </button>
      </div>
    </>
  );
};

export default CartItem;
